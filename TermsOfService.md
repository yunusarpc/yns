  APP STORE HİZMET ŞARTLARI

Son Güncelleme: 01.03.2021

Bu hizmet şartları ("Şartlar"), App Store'a erişiminiz ve kullanımınız için geçerlidir.  Lütfen bunları dikkatlice okuyun.

  ŞARTLARI KABUL ETME

Hizmete erişir veya Hizmet'i kullanırsanız, bu, aşağıdaki tüm şartlara bağlı kalmayı kabul ettiğiniz anlamına gelir.  Bu nedenle, Hizmeti kullanmadan önce lütfen tüm şartları okuyun.  Aşağıdaki tüm şartları kabul etmiyorsanız, lütfen Hizmeti kullanmayın.  Ayrıca, bir terim size mantıklı gelmiyorsa, lütfen yunusarpc@gmail.com adresine e-posta göndererek bize bildirin.

  ŞARTLARDAKİ DEĞİŞİKLİKLER

Bu Koşulları herhangi bir zamanda değiştirme hakkını saklı tutarız.  Örneğin, yeni bir özellikle çıkarsak veya başka bir nedenle bu Koşulları değiştirmemiz gerekebilir.

Bu Şartlarda değişiklik yaptığımızda, değişiklikler 01.03.2021 bu tür revize edilmiş Şartları yayınladıktan sonra (bu Şartların üst kısmındaki tarihi revize ederek belirtilir) veya acil bildiriminiz için bir mekanizma sağlarsak kabul etmeniz üzerine yürürlüğe girer.  revize edilmiş Şartların kabulü (tıklamalı onay veya kabul düğmesi gibi).  Bu Koşullardaki değişiklikler için App Store'u kontrol etmek sizin sorumluluğunuzdadır.

Revize edilmiş Şartlar yürürlüğe girdikten sonra Hizmeti kullanmaya devam ederseniz, bu Şartlarda yapılan değişiklikleri kabul etmiş olursunuz.

  TELİF HAKKI POLİTİKASI

Fikri mülkiyet haklarını ciddiye alıyoruz ve GNU GENEL KAMU LİSANSI Sürüm 3'e uygun bir telif hakkı politikası kullanarak, uygun koşullarda ve tamamen kendi takdirimize bağlı olarak, kullanıcıların hizmete erişimini sonlandırmak için bir politika benimsedik.  tekrar ihlal edenler olarak kabul edilir.  App Store projesini çatallamak veya depodan kod kopyalamak isteyen kişiler, kullanılan lisansa UYMALIDIR. 

  ÜÇÜNCÜ TARAF HİZMETLERİ

Kullanıcılara iki büyük üçüncü taraf hizmeti sunuyoruz.  Hizmeti kullanımınız, bir üçüncü tarafın geliştirdiği veya sahibi olduğu uygulamaların kullanımını da içerebilir.  Bu tür üçüncü taraf uygulamalarını, web sitelerini ve hizmetlerini kullanımınız, o tarafın kendi hizmet şartlarına veya gizlilik politikalarına tabidir.  Ziyaret ettiğiniz veya kullandığınız herhangi bir üçüncü taraf uygulamasının, web sitesinin veya hizmetinin hüküm ve koşullarını ve gizlilik politikasını okumanızı öneririz.

  ANONİM HESAP SUNUCUSU

Kullanıcılara, sunucumuzdan belirteç olarak sağlanan App Store içindeki anonim hesapları kullanma olanağı sağlıyoruz.  Hizmeti kullanımınız, sunucumuzu herhangi bir şekilde zararlı veya kötü niyetli olarak etkilememeyi kabul eder.

Zaman zaman katkıda bulunanlardan bize hesap vermelerini isteyebiliriz.  App Store için oluşturduğunuz hesaplara App Store'a tam erişim verilmesini ve sunucumuzda saklanmasını burada kabul etmektesiniz.  Bu, anonim olmayan hesaplar için geçerli değildir.

  APP STORE MATERYALLER

Logo ve tüm tasarımlar, metin, grafikler, resimler, bilgiler ve diğer içerikler (App Store içindeki içerik hariç) dahil olmak üzere Hizmeti oluşturmak için çok çaba harcadık.  Bu mülk bize veya lisans verenlerimize aittir ve açık kaynak lisansları ile korunmaktadır.  Size kullanım hakkı veriyoruz.

  KAÇINILMAZ YASAL KONULAR

HİZMET VE HİZMET ARACILIĞIYLA SİZE DAHİL OLAN VEYA BAŞKA BİR ŞEKİLDE SUNULAN DİĞER HİZMETLER VE İÇERİK, SİZE HERHANGİ BİR BEYAN VEYA GARANTİ VERİLMEKSİZİN OLDUĞU GİBİ VEYA MEVCUT OLDUĞU GİBİ SAĞLANIR.  HİZMET ÜZERİNDE BULUNAN YA DA BAŞKA BİR ŞEKİLDE HİZMET ARACILIĞIYLA SİZE SUNULAN HİZMET VE İÇERİK İLE İLGİLİ TÜM GARANTİLERİ VE BEYANLARI (AÇIK VEYA ZIMNİ, SÖZLÜ VEYA YAZILI) REDDEDERİZ.  TİCARETTE, ALIŞVERİŞ YOLUYLA VEYA BAŞKA BİR ŞEKİLDE.

App OSS HİÇBİR DURUMDA HİZMETTEN VEYA DİĞER HERHANGİ BİR HİZMET VE/VEYA İÇERİKLE BAĞLANTILI OLARAK KAYNAKLANAN HİÇBİR ÖZEL, DOLAYLI, ARIZİ, ÖRNEK VEYA DOLAYLI ZARARLARDAN HİÇBİR DURUMDA SİZE VEYA HERHANGİ BİR ÜÇÜNCÜ TARAFA SORUMLU OLMAYACAKTIR.  BU TÜR ZARARLARIN OLASILIĞI KONUSUNDA BİLGİLENDİRİLMİŞ VEYA BU TÜR ZARARLARIN OLASILIĞININ FARKINDA OLMUŞ OLSA DAHİ, SÖZLEŞMEDE, HAKSIZ SORUMLULUKTA, KUSURSUZ SORUMLULUKTA VEYA DİĞER HUSUSLARDA EYLEM ŞEKLİNE BAKMAYARAK, HİZMET ARACILIĞIYLA SİZE DAHİL OLMUŞ VEYA BAŞKA BİR ŞEKİLDE SUNULMUŞ OLANLAR  ZARARLAR.  TÜM EYLEM SEBEPLERİNDE VE TÜM SORUMLULUK TEORİLERİ KAPSAMINDA TOPLAM SORUMLULUĞUMUZ, App OSS'E ÖDEDİĞİNİZ MİKTARLA SINIRLI OLACAKTIR.  BU SÖZLEŞMEDE BELİRTİLEN HERHANGİ BİR ÇÖZÜM YOLUNUN ESAS AMACIYLA İLGİLİ OLMADIĞI DÜŞÜNÜLMESİ DURUMUNDA BU BÖLÜM TAM YÜRÜRLÜĞE SAHİP OLACAKTIR.

ile ilgili olarak maruz kaldığımız tüm masraflara, zararlara, yükümlülüklere ve harcamalara (avukat ücretleri, masraflar, cezalar, faiz ve harcamalar dahil) karşı bizi savunmayı, tazmin etmeyi ve zarar görmemizi engellemeyi kabul edersiniz.  Hizmet kullanımınızın geçerli herhangi bir yasa veya düzenlemeyi ihlal ettiğine dair herhangi bir iddia da dahil olmak üzere, Hizmet'i kullanımınızla veya Hesabınızı kullanan herhangi bir kişi tarafından Hizmet'in kullanımıyla ilgili olarak üçüncü bir taraftan gelen herhangi bir iddia veya talepten kaçınmak amacıyla  herhangi bir üçüncü tarafın hakları ve/veya bu Şartları ihlal etmeniz.

  SONLANDIRMA

Bu Koşullardan herhangi birini ihlal ederseniz, Hizmete erişiminizi veya Hizmeti kullanımınızı askıya alma veya devre dışı bırakma hakkına sahibiz.

  TÜM ANLAŞMA

Bu Koşullar, Hizmetin kullanımıyla ilgili olarak App Store ile aranızdaki sözleşmenin tamamını oluşturur ve Hizmeti kullanımınızla ilgili olarak App Store ile aranızdaki önceki tüm sözleşmelerin yerine geçer.

  GERİ BİLDİRİM

Lütfen Hizmet, bu Koşullar ve genel olarak App Store hakkındaki düşüncelerinizi bize bildirin.  Bize Hizmet, bu Şartlar ve genel olarak App Store hakkında herhangi bir geri bildirim, yorum veya öneri sağladığınızda, geri bildiriminiz, yorumlarınız ve bunlara ilişkin tüm haklarınızı, unvanlarınızı ve menfaatlerinizi gayrikabili rücu olarak bize devretmiş olursunuz.  öneriler.

Herhangi bir Hizmet güvenlik ihlali keşfeder veya bundan şüphelenirseniz, lütfen mümkün olan en kısa sürede bize bildirin.

  SORULAR VE İLETİŞİM BİLGİLERİMİZ

Hizmetle ilgili sorular veya yorumlar yunusarpc@gmail.com e-posta adresinden bize yönlendirilebilir.